Dependencies
------------
- cython
- numpy v1.8 or later
- netCDF4
- pyproj
- PYTHON_ufz_lib

Installation
------------
python setup.py build_ext --inplace

Documentation
-------------
The main input file `input.yml` is documented and should (hopefully) give an overview
